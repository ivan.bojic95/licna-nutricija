<?php
// Initialize the session
session_start();

// if the user is already logged in, if yes then redirect him to welcome page
//if(isset($_SESSION["loggedin"]) == true){
 //header("location: ./Uloguj_se.php");
 //   exit;
//}

// Include config file
require_once "db.php";

// Define variables and initialize with empty values
$email = $lozinka = "";
$email_err = $lozinka_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    
    // Check if username is empty
    if(empty(trim($_POST["email"]))){
        $email_err = "Unesite email.";
    } else{
        $email = trim($_POST["email"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["lozinka"]))){
        $lozinka_err = "Unesite lozinku";
    } else{
        $lozinka = trim($_POST["lozinka"]);
    }
    
    // Validate credentials
    if(empty($email_err) && empty($lozinka_err)){
        // Prepare a select statement
        $sql = "SELECT id, email, lozinka FROM korisnici WHERE email = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = $email;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
                
                // Check if username exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $email, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($lozinka, $hashed_password)){
                            // Password is correct, so start a new session
                            session_start();
                            
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $email;
                            
                            // Redirect user to welcome page
                            header("location: User.php");
                        } else{
                            // Display an error message if password is not valid
                            $lozinka = "Lozinka nije validna.";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $email_err = "Nismo pronasli nalog sa tim emailom.";
                }
            } else{
                echo "Nesto nije u redu, pokusajte kasnije opet..";
            }
            
            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Close connection
    mysqli_close($link);
}
//forgot password

?>
<!DOCTYPE html>
<head>
<title>Ulogujte se</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="templates/css/nav.css">
</head>
<body>


<div class="content" align="center" style="width: 100%; height: 100%; background-image: url('templates/img/pozadinahrana.jpg'); background-size: 120%;">
<h4 style="color:white; float: left; ">Vreme je da zapocnete zdraviji zivot!</h4>

<header><?php include './header.php';?></header>


<div class="content" style=" float:left; margin-right:7px; margin-top:10px; width:20%; height:100%;">
<div style="overflow:auto">
  <div class="menu">
     
  </div>
</div>
	
	
</div>
<div class="content" style=" float:right; margin-top:10px; margin-bottom:5px; width:77%; height:100%;">
<h2>Prijavi se</h2>
        <p>Popunite polja da bi se prijavili.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                <label>Email</label>
                <input type="email" name="email" class="form-control" value="<?php echo $email; ?>">
                <span class="help-block"><?php echo $email_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($lozinka_err)) ? 'has-error' : ''; ?>">
                <label>Lozinka</label>
                <input type="password" name="lozinka" class="form-control">
                <span class="help-block"><?php echo $lozinka_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Prijavi se" style="background-color: #4e9a06;"">
            </div>
            <p>Jos nemas nalog? <a href="Registruj_se.php">Registruj se ovde!</a>.</p>
            <a href="./zaboravljenalozinka/zaboravljenalozinka.php">Zaboravio si lozinku? </a>
            <br><br><br><br><br><br><br>
        </form>
		
	</div>	
	
</form>
<footer><?php include './footer.php';?></footer>
</div>
<br><br><br><br><br><br><br>
</div>
</div>









</body>
</html>