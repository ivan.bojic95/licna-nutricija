<?php
$connect = mysqli_connect("localhost", "root", "", "Nutricija");

/* Check the connection. */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

?>
<!DOCTYPE html>
<head>
<title>Profil</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="templates/css/nav.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pure/1.0.1/pure-min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #4e9a06;
}


body
    {
      margin:0;
      padding:0;
      background-color:#f1f1f1;
    }
    .box
    {
      width:900px;
      padding:20px;
      background-color:#fff;
      border:1px solid #ccc;
      border-radius:5px;
      margin-top:10px;
    }

</style>


<div class="content" align="center" style="width: 100%; height: 100%; background-image: url('templates/img/backfood.jpg'); 
background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">
<h4 style="color:white; float: left; ">Vreme je da zapocnete zdraviji zivot!</h4>

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    </head>  
</head>

<body>

<div class="header" >
<a href="Uindex.php"><img src="templates/img/logopg.png" alt="logo" 
 style=" width:50px; align:left; ">Nutricionista  </a>
  <div class="header-right">
    <a  href="./Uindex.php">Pocetna</a>
    <a  class="active" href="./User.php">Profil</a>
    <a href="./UKontakt.php">Kontakt</a>
    <a  href="./UO_nama.php">O nama</a>
    <a href="odjavise.php" class="btn btn-danger">Odjavite se</a>
  </div>
</div>
 
 

<div class="content" style=" float:left; margin-right:7px; margin-top:2px; width:20%; height:100%;">
<div style="overflow:auto">
  <div class="menu">
  </div>
</div>
	
	
</div>
<div class="content" style=" float:right; margin-top:10px; margin-bottom:5px; width:77%; height:100%;">
<div class=" text-center">
  <h1>Profil</h1>
</div>


<div class="content" heigh="100%">  
            
			<div class="table-responsive">  
			<div id="live_data"></div>
				<span id="result"></span>
				<div id="live_data"></div>                 
			</div>  
		</div>
 
<script>  
$(document).ready(function(){  
    function fetch_data()  
    {  
        $.ajax({  
            url:"./config/select.php",  
            method:"POST",  
            success:function(data){  
				$('#live_data').html(data);  
            }  
        });  
    }  
    fetch_data();  
    $(document).on('click', '#btn_add', function(){  
        var naziv = $('#naziv').text();  
        var kolicina = $('#kolicina').text();  
        var kalorije = $('#kalorije').text(); 
        var masti = $('#masti').text(); 
        var proteini = $('#proteini').text(); 
        var ugljenihidrati = $('#ugljenihidrati').text(); 
        var datum = $('#datum').text(); 
        if(naziv == '')  
        {  
            alert("Unesite naziv");  
            return false;  
        }  
        if(kolicina == '')  
        {  
            alert("Unesite kolicinu.");  
            return false;  
        }  
        if(kalorije == '')  
        {  
            alert("Unesite kalorije.");  
            return false;  
        } 
        if(masti == '')  
        {  
            alert("Unesite masti.");  
            return false;  
        } 
        if(proteini == '')  
        {  
            alert("Unesite proteini.");  
            return false;  
        } 
        if(ugljenihidrati == '')  
        {  
            alert("Unesite ugljenehidrate.");  
            return false;  
        }
        if(datum == '')  
        {  
            alert("Unesite datum.");  
            return false;  
        }  
        $.ajax({  
            url:"./config/insert.php",  
            method:"POST",  
            data:{ naziv:naziv, kolicina:kolicina, kalorije:kalorije , masti:masti, proteini:proteini, ugljenihidrati:ugljenihidrati, datum:datum},  
            dataType:"text",  
            success:function(data)  
            {  
                alert(data);  
                fetch_data();  
            }  
        })  
    });  
    
	function edit_data(id, text, column_name)  
    {  
        $.ajax({  
            url:"./config/edit.php",  
            method:"POST",  
            data:{id:id, text:text, column_name:column_name},  
            dataType:"text",  
            success:function(data){  
                //alert(data);
				//$('#result').html("<div class='alert alert-success'>"+data+"</div>");
            }  
        });  
    }  
    
    $(document).on('blur', '.naziv', function(){  
        var id = $(this).data("id1");  
        var naziv = $(this).text();  
        edit_data(id, naziv, "naziv");  
    });  
    $(document).on('blur', '.kolicina', function(){  
        var id = $(this).data("id2");  
        var kolicina = $(this).text();  
        edit_data(id,naziv, "kolicina");  
    });  
    $(document).on('blur', '.kalorije', function(){  
        var id = $(this).data("id1");  
        var kalorije = $(this).text();  
        edit_data(id, kalorije, "kalorije");  
    });
    $(document).on('blur', '.masti', function(){  
        var id = $(this).data("id1");  
        var masti = $(this).text();  
        edit_data(id, masti, "masti");  
    }); 
    $(document).on('blur', '.proteini', function(){  
        var id = $(this).data("id1");  
        var proteini = $(this).text();  
        edit_data(id, proteini, "proteini");  
    });   
    $(document).on('blur', '.ugljenihidrati', function(){  
        var id = $(this).data("id1");  
        var ugljenihidrati = $(this).text();  
        edit_data(id, ugljenihidrati, "ugljenihidrati");  
    }); 
    $(document).on('blur', '.datum', function(){  
        var id = $(this).data("id1");  
        var datum = $(this).text();  
        edit_data(id, datum, "datum");  
    });  
    $(document).on('click', '.btn_delete', function(){  
        var id=$(this).data("id3");  
        if(confirm("Da li ste sigurni da zelite da izbrisete?"))  
        {  
            $.ajax({  
                url:"./config/delete.php",  
                method:"POST",  
                data:{id:id},  
                dataType:"text",  
                success:function(data){  
                    alert(data);  
                    fetch_data();  
                }  
            });  
        }  
    });  
});  


/////// 

</script>
 <?Php
require "db.php";// Database connection

if($stmt = $connect->query("SELECT datum,SUM(masti),SUM(kalorije),SUM(proteini),SUM(ugljenihidrati) FROM hrana ORDER by datum ASC")){

 // echo "No of records : ".$stmt->num_rows."<br>";
$php_data_array = Array(); // create PHP array
 // echo "<table>
//<tr> <th>Datum</th><th>Naziv</th><th>Kalorija</th></tr>";
while ($row = $stmt->fetch_row()) {
  // echo "<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>$row[3]</tr>";
   $php_data_array[] = $row; // Adding to array
   }
//echo "</table>";
}else{
echo $connect->error;
}
//print_r( $php_data_array);
// You can display the json_encode output here. 
// echo json_encode($php_data_array); 

// Transfor PHP array to JavaScript two dimensional array 
echo "<script>
      var my_2d = ".json_encode($php_data_array)."
</script>";
?>


<div id="chart_div" class="content" style="  margin-top:10px; margin-bottom:5px; width:100%; height:100%;">
</div>
<br><br><br><br><br>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {packages: ['corechart', 'bar']});
      google.charts.setOnLoadCallback(drawChart);
	  
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'datum');
        data.addColumn('number', 'masti');
		data.addColumn('number', 'kalorije');
		data.addColumn('number', 'proteini');
		data.addColumn('number', 'ugljenihidrati');
        for(i = 0; i < my_2d.length; i++)
    data.addRow([my_2d[i][0], parseInt(my_2d[i][1]),parseInt(my_2d[i][2]),parseInt(my_2d[i][3]),parseInt(my_2d[i][4])]);
       var options = {
          title: 'plus2net.com hrana Kalorije',
          hAxis: {title: 'Datum',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));
        chart.draw(data, options);
       }
	///////////////////////////////
////////////////////////////////////	
</script>
<br><br>	
<h2>Tabela kalorija sadrzana u namirnicama</h2>
<br><h4><b>MLEKO I MLECNI PROIZVODI</b></h4>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
		  <tbody>
		    <tr>
		      <td width="148" height="20"><strong>Namirnica (100 g)</strong></td>
		      <td colspan="2" align="center"><strong>Energija</strong></td>
		      <td width="34" align="center"><strong>UH</strong></td>
		      <td width="105" align="center"><strong>Belančevine</strong></td>
		      <td width="73" align="center"><strong>Masti</strong></td>
	        </tr>
		    <tr>
		      <td height="22"><b>MLEKO I MLECNI PROIZVODI</b></td>
		      <td width="34" align="center">kJ</td>
		      <td width="36" align="center">kcal</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
	        </tr>
		    <tr>
		      <td height="22">Mleko (0,9 % masti)</td>
		      <td align="center">167</td>
		      <td align="center">40</td>
		      <td align="center">4,7</td>
		      <td align="center">3,3</td>
		      <td align="center">0,9</td>
	        </tr>
		    <tr>
		      <td height="22">Mleko (3,2 % masti)</td>
		      <td align="center">275</td>
		      <td align="center">66</td>
		      <td align="center">4,7</td>
		      <td align="center">3,3</td>
		      <td align="center">3,2</td>
	        </tr>
		    <tr>
		      <td height="22">Jogurt (obični)</td>
		      <td align="center">360</td>
		      <td align="center">40</td>
		      <td align="center">5</td>
		      <td align="center">4</td>
		      <td align="center">4</td>
	        </tr>
		    <tr>
		      <td height="22">Kisela pavlaka</td>
		      <td align="center">800</td>
		      <td align="center">192</td>
		      <td align="center">3</td>
		      <td align="center">3</td>
		      <td align="center">18</td>
	        </tr>
		    <tr>
		      <td height="22">Slatka pavlaka</td>
		      <td align="center">1300</td>
		      <td align="center">317</td>
		      <td align="center">2</td>
		      <td align="center">3</td>
		      <td align="center">32</td>
	        </tr>
		    <tr>
		      <td height="22">Puding od čokolade</td>
		      <td align="center">560</td>
		      <td align="center">134</td>
		      <td align="center">21</td>
		      <td align="center">3,5</td>
		      <td align="center">4</td>
	        </tr>
		    <tr>
		      <td height="22">Sirni namazi (23% masti)</td>
		      <td align="center">480</td>
		      <td align="center">115</td>
		      <td align="center">6</td>
		      <td align="center">13</td>
		      <td align="center">5</td>
	        </tr>
		    <tr>
		      <td height="22">Topljeni sir (45 % masti)</td>
		      <td align="center">1275</td>
		      <td align="center">385</td>
		      <td align="center">6</td>
		      <td align="center">14</td>
		      <td align="center">24</td>
	        </tr>
		    <tr>
		      <td height="22">Tvrdi sir (45% masti)</td>
		      <td align="center">1555</td>
		      <td align="center">372</td>
		      <td align="center">3</td>
		      <td align="center">25</td>
		      <td align="center">28</td>
	        </tr>
		    <tr>
		      <td height="22">Sveži kravlji sir</td>
		      <td align="center">101</td>
		      <td align="center">72</td>
		      <td align="center">4</td>
		      <td align="center">15</td>
		      <td align="center">3</td>
	        </tr>
	      </tbody>
	  </table>
<br><h4><b>MESO I MESNE PRERADJEVINE</b></h4>
<tbody>
<table>
		    <tr>
		      <td width="148" height="20"><strong>Namirnica (100 g)</strong></td>
		      <td colspan="2" align="center"><strong>Energija</strong></td>
		      <td width="33" align="center"><strong>UH</strong></td>
		      <td width="106" align="center"><strong>Belančevine</strong></td>
		      <td width="73" align="center"><strong>Masti</strong></td>
	        </tr>
		    <tr>
		      <td height="10"><b>MESO I MESNE PRERADJEVINE </b></td>
		      <td width="35" align="center">kJ</td>
		      <td width="35" align="center">kcal</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
	        </tr>
		    <tr>
		      <td height="22">Bubrezi (teleći)</td>
		      <td align="center">505</td>
		      <td align="center">121</td>
		      <td align="center">1</td>
		      <td align="center">15</td>
		      <td align="center">6</td>
	        </tr>
		    <tr>
		      <td height="22">Viršle (govedina + svinjetina)</td>
		      <td align="center">1350</td>
		      <td align="center">320</td>
		      <td align="center">2</td>
		      <td align="center">11</td>
		      <td align="center">29</td>
	        </tr>
		    <tr>
		      <td height="22">Viršle (pileće)</td>
		      <td align="center">1080</td>
		      <td align="center">258</td>
		      <td align="center">7</td>
		      <td align="center">13</td>
		      <td align="center">20</td>
	        </tr>
		    <tr>
		      <td height="22">Janjetina (srednje masna)</td>
		      <td align="center">875</td>
		      <td align="center">211</td>
		      <td align="center">0</td>
		      <td align="center">19</td>
		      <td align="center">15</td>
	        </tr>
		    <tr>
		      <td height="22">Jetra (teleća)</td>
		      <td align="center">575</td>
		      <td align="center">137</td>
		      <td align="center">4</td>
		      <td align="center">18</td>
		      <td align="center">4</td>
	        </tr>
		    <tr>
		      <td height="22">Jetrena pašteta</td>
		      <td align="center">1860</td>
		      <td align="center">440</td>
		      <td align="center">1</td>
		      <td align="center">12</td>
		      <td align="center">40</td>
	        </tr>
		    <tr>
		      <td height="22">Kobasica (prosečno)</td>
		      <td align="center">1355</td>
		      <td align="center">324</td>
		      <td align="center">1</td>
		      <td align="center">11</td>
		      <td align="center">30</td>
	        </tr>
		    <tr>
		      <td height="22">Konjetina</td>
		      <td align="center">370</td>
		      <td align="center">89</td>
		      <td align="center">0</td>
		      <td align="center">16</td>
		      <td align="center">2</td>
	        </tr>
		    <tr>
		      <td height="22">Krvavice</td>
		      <td align="center">1780</td>
		      <td align="center">424</td>
		      <td align="center">0</td>
		      <td align="center">13</td>
		      <td align="center">39</td>
	        </tr>
		    <tr>
		      <td height="22">Mesni narezak (svinjsko meso)</td>
		      <td align="center">1780</td>
		      <td align="center">424</td>
		      <td align="center">4</td>
		      <td align="center">12</td>
		      <td align="center">40</td>
	        </tr>
		    <tr>
		      <td height="22">Mleveno, mešano meso</td>
		      <td align="center">1060</td>
		      <td align="center">253</td>
		      <td align="center">0</td>
		      <td align="center">20</td>
		      <td align="center">19</td>
	        </tr>
		    <tr>
		      <td height="22">Ovčetina</td>
		      <td align="center">1050</td>
		      <td align="center">246</td>
		      <td align="center">0</td>
		      <td align="center">13</td>
		      <td align="center">24</td>
	        </tr>
		    <tr>
		      <td height="22">Piletina (belo meso bez kostiju)</td>
		      <td align="center">600</td>
		      <td align="center">144</td>
		      <td align="center">0</td>
		      <td align="center">21</td>
		      <td align="center">3</td>
	        </tr>
		    <tr>
		      <td height="22">Ćuretina (belo meso bez kostiju)</td>
		      <td align="center">970</td>
		      <td align="center">231</td>
		      <td align="center">0</td>
		      <td align="center">22</td>
		      <td align="center">5</td>
	        </tr>
		    <tr>
		      <td height="22">Salama parizer</td>
		      <td align="center">2190</td>
		      <td align="center">523</td>
		      <td align="center">1</td>
		      <td align="center">17</td>
		      <td align="center">47</td>
	        </tr>
		    <tr>
		      <td height="22">Salama, pileća, ćureća</td>
		      <td align="center">820</td>
		      <td align="center">197</td>
		      <td align="center">1</td>
		      <td align="center">16</td>
		      <td align="center">14</td>
	        </tr>
		    <tr>
		      <td height="22">Slanina</td>
		      <td align="center">2530</td>
		      <td align="center">605</td>
		      <td align="center">0</td>
		      <td align="center">8</td>
		      <td align="center">60</td>
	        </tr>
		    <tr>
		      <td height="22">Srnetina</td>
		      <td align="center">515</td>
		      <td align="center">123</td>
		      <td align="center">0</td>
		      <td align="center">21</td>
		      <td align="center">3</td>
	        </tr>
		    <tr>
		      <td height="22">Svinjetina</td>
		      <td align="center">1445</td>
		      <td align="center">345</td>
		      <td align="center">0</td>
		      <td align="center">18</td>
		      <td align="center">27</td>
	        </tr>
		    <tr>
		      <td height="22">Šunka dimljena i pršuta</td>
		      <td align="center">1653</td>
		      <td align="center">385</td>
		      <td align="center">0</td>
		      <td align="center">18</td>
		      <td align="center">33</td>
	        </tr>
		    <tr>
		      <td height="22">Šunka (kuvana)</td>
		      <td align="center">1145</td>
		      <td align="center">274</td>
		      <td align="center">0</td>
		      <td align="center">19</td>
		      <td align="center">20</td>
	        </tr>
		    <tr>
		      <td height="22">Šunka ćureća/pileća</td>
		      <td align="center">525</td>
		      <td align="center">128</td>
		      <td align="center">0</td>
		      <td align="center">19</td>
		      <td align="center">5</td>
	        </tr>
		    <tr>
		      <td height="22">Teletina</td>
		      <td align="center">390</td>
		      <td align="center">105</td>
		      <td align="center">0</td>
		      <td align="center">21</td>
		      <td align="center">3</td>
	        </tr>
	        </table>
	      </tbody>
<br><h4><b>RIBA</b></h4>
<tbody>
<table>
		    <tr>
		      <td width="148" height="20"><strong>Namirnica (100 g)</strong></td>
		      <td colspan="2" align="center"><strong>Energija</strong></td>
		      <td width="34" align="center"><strong>UH</strong></td>
		      <td width="105" align="center"><strong>Belančevine</strong></td>
		      <td width="73" align="center"><strong>Masti</strong></td>
	        </tr>
		    <tr>
		      <td height="10"><b>RIBA</b></td>
		      <td width="34" align="center">kJ</td>
		      <td width="36" align="center">kcal</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
	        </tr>
		    <tr>
		      <td height="22">Bakalar</td>
		      <td align="center">295</td>
		      <td align="center">76</td>
		      <td align="center">0</td>
		      <td align="center">17</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Dagnja</td>
		      <td align="center">270</td>
		      <td align="center">66</td>
		      <td align="center">2</td>
		      <td align="center">12</td>
		      <td align="center">2</td>
	        </tr>
		    <tr>
		      <td height="22">Grgeč</td>
		      <td align="center">295</td>
		      <td align="center">75</td>
		      <td align="center">0</td>
		      <td align="center">15</td>
		      <td align="center">2</td>
	        </tr>
		    <tr>
		      <td height="22">Haringa</td>
		      <td align="center">650</td>
		      <td align="center">155</td>
		      <td align="center">0</td>
		      <td align="center">13</td>
		      <td align="center">10</td>
	        </tr>
		    <tr>
		      <td height="22">Inćun</td>
		      <td align="center">310</td>
		      <td align="center">89</td>
		      <td align="center">0</td>
		      <td align="center">17</td>
		      <td align="center">3</td>
	        </tr>
		    <tr>
		      <td height="22">Jastog</td>
		      <td align="center">305</td>
		      <td align="center">86</td>
		      <td align="center">1</td>
		      <td align="center">16</td>
		      <td align="center">2</td>
	        </tr>
		    <tr>
		      <td height="22">Jegulja</td>
		      <td align="center">875</td>
		      <td align="center">209</td>
		      <td align="center">1</td>
		      <td align="center">9</td>
		      <td align="center">18</td>
	        </tr>
		    <tr>
		      <td height="22">Kamenica (ostriga)</td>
		      <td align="center">225</td>
		      <td align="center">49</td>
		      <td align="center">4</td>
		      <td align="center">6</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Lignja</td>
		      <td align="center">295</td>
		      <td align="center">77</td>
		      <td align="center">1</td>
		      <td align="center">16</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Losos</td>
		      <td align="center">910</td>
		      <td align="center">217</td>
		      <td align="center">0</td>
		      <td align="center">20</td>
		      <td align="center">14</td>
	        </tr>
		    <tr>
		      <td height="22">Pastrva</td>
		      <td align="center">470</td>
		      <td align="center">112</td>
		      <td align="center">0</td>
		      <td align="center">18</td>
		      <td align="center">2</td>
	        </tr>
		    <tr>
		      <td height="22">Sardine u ulju</td>
		      <td align="center">1005</td>
		      <td align="center">240</td>
		      <td align="center">1</td>
		      <td align="center">24</td>
		      <td align="center">14</td>
	        </tr>
		    <tr>
		      <td height="22">Skuša</td>
		      <td align="center">820</td>
		      <td align="center">195</td>
		      <td align="center">0</td>
		      <td align="center">19</td>
		      <td align="center">12</td>
	        </tr>
		    <tr>
		      <td height="22">Šaran</td>
		      <td align="center">270</td>
		      <td align="center">65</td>
		      <td align="center">0</td>
		      <td align="center">10</td>
		      <td align="center">3</td>
	        </tr>
		    <tr>
		      <td height="22">Škampi</td>
		      <td align="center">310</td>
		      <td align="center">91</td>
		      <td align="center">0</td>
		      <td align="center">17</td>
		      <td align="center">2</td>
	        </tr>
		    <tr>
		      <td height="22">Štuka</td>
		      <td align="center">305</td>
		      <td align="center">85</td>
		      <td align="center">0</td>
		      <td align="center">17</td>
		      <td align="center">2</td>
	        </tr>
		    <tr>
		      <td height="22">Tunjevina u ulju</td>
		      <td align="center">1270</td>
		      <td align="center">303</td>
		      <td align="center">0</td>
		      <td align="center">24</td>
		      <td align="center">21</td>
	        </tr>
	        </table>
	      </tbody>
<br><h4><b>ZITARICE</b></h4>
<tbody>
<table>
		    <tr>
		      <td width="148" height="20"><strong>Namirnica (100 g)</strong></td>
		      <td colspan="2" align="center"><strong>Energija</strong></td>
		      <td width="34" align="center"><strong>UH</strong></td>
		      <td width="105" align="center"><strong>Belančevine</strong></td>
		      <td width="73" align="center"><strong>Masti</strong></td>
	        </tr>
		    <tr>
		      <td height="10"><b>ZITARICE</b></td>
		      <td width="34" align="center">kJ</td>
		      <td width="36" align="center">kcal</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
	        </tr>
		    <tr>
		      <td height="22">Crni hleb</td>
		      <td align="center">1046</td>
		      <td align="center">250</td>
		      <td align="center">51</td>
		      <td align="center">6</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Dvopek</td>
		      <td align="center">1590</td>
		      <td align="center">397</td>
		      <td align="center">77</td>
		      <td align="center">10</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Griz</td>
		      <td align="center">1550</td>
		      <td align="center">370</td>
		      <td align="center">75</td>
		      <td align="center">10</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Kolači od samog testa</td>
		      <td align="center">1315</td>
		      <td align="center">314</td>
		      <td align="center">39</td>
		      <td align="center">7</td>
		      <td align="center">13</td>
	        </tr>
		    <tr>
		      <td height="22">Kokice</td>
		      <td align="center">1580</td>
		      <td align="center">376</td>
		      <td align="center">72</td>
		      <td align="center">13</td>
		      <td align="center">4</td>
	        </tr>
		    <tr>
		      <td height="22">Hleb sa celim zrnima</td>
		      <td align="center">1004</td>
		      <td align="center">240</td>
		      <td align="center">46</td>
		      <td align="center">7</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Kukuruzni hleb</td>
		      <td align="center">915</td>
		      <td align="center">220</td>
		      <td align="center">31</td>
		      <td align="center">5</td>
		      <td align="center">9</td>
	        </tr>
		    <tr>
		      <td height="22">Kukuruzne pahuljice</td>
		      <td align="center">1625</td>
		      <td align="center">388</td>
		      <td align="center">83</td>
		      <td align="center">6</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Musli</td>
		      <td align="center">1550</td>
		      <td align="center">371</td>
		      <td align="center">68</td>
		      <td align="center">11</td>
		      <td align="center">6</td>
	        </tr>
		    <tr>
		      <td height="22">Polubeli hleb</td>
		      <td align="center">1055</td>
		      <td align="center">252</td>
		      <td align="center">52</td>
		      <td align="center">3</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Pšenično brašno</td>
		      <td align="center">1550</td>
		      <td align="center">370</td>
		      <td align="center">71</td>
		      <td align="center">12</td>
		      <td align="center">2</td>
	        </tr>
		    <tr>
		      <td height="22">Raženo brašno</td>
		      <td align="center">1490</td>
		      <td align="center">356</td>
		      <td align="center">35</td>
		      <td align="center">9</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Riža ljuštena</td>
		      <td align="center">1540</td>
		      <td align="center">368</td>
		      <td align="center">79</td>
		      <td align="center">7</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Riža neljuštena</td>
		      <td align="center">1550</td>
		      <td align="center">371</td>
		      <td align="center">75</td>
		      <td align="center">7</td>
		      <td align="center">2</td>
	        </tr>
		    <tr>
		      <td height="22">Soja u zrnu</td>
		      <td align="center">1785</td>
		      <td align="center">427</td>
		      <td align="center">26</td>
		      <td align="center">38</td>
		      <td align="center">19</td>
	        </tr>
		    <tr>
		      <td height="22">Sojin sir (tofu)</td>
		      <td align="center">285</td>
		      <td align="center">72</td>
		      <td align="center">2</td>
		      <td align="center">8</td>
		      <td align="center">4</td>
	        </tr>
		    <tr>
		      <td height="22">Testenina sa jajima</td>
		      <td align="center">1630</td>
		      <td align="center">390</td>
		      <td align="center">72</td>
		      <td align="center">13</td>
		      <td align="center">3</td>
	        </tr>
		    <tr>
		      <td height="22">Zobene pahuljice</td>
		      <td align="center">1680</td>
		      <td align="center">402</td>
		      <td align="center">66</td>
		      <td align="center">14</td>
		      <td align="center">7</td>
	        </tr>
	        </table>
	      </tbody>
<br><h4><b>VOCE</b></h4>
<tbody>
<table>
		    <tr>
		      <td width="148" height="20"><strong>Namirnica (100 g)</strong></td>
		      <td colspan="2" align="center"><strong>Energija</strong></td>
		      <td width="34" align="center"><strong>UH</strong></td>
		      <td width="105" align="center"><strong>Belančevine</strong></td>
		      <td width="73" align="center"><strong>Masti</strong></td>
	        </tr>
		    <tr>
		      <td height="10">VOCE</td>
		      <td width="34" align="center">kJ</td>
		      <td width="36" align="center">kcal</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
	        </tr>
		    <tr>
		      <td height="22">Ananas</td>
		      <td align="center">230</td>
		      <td align="center">56</td>
		      <td align="center">13</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Banane</td>
		      <td align="center">410</td>
		      <td align="center">99</td>
		      <td align="center">23</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Borovnice</td>
		      <td align="center">260</td>
		      <td align="center">62</td>
		      <td align="center">14</td>
		      <td align="center">1</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Breskve</td>
		      <td align="center">192</td>
		      <td align="center">46</td>
		      <td align="center">11</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Dinje</td>
		      <td align="center">100</td>
		      <td align="center">24</td>
		      <td align="center">5</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Grožđe</td>
		      <td align="center">295</td>
		      <td align="center">70</td>
		      <td align="center">16</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Grejp</td>
		      <td align="center">180</td>
		      <td align="center">42</td>
		      <td align="center">10</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Jabuka</td>
		      <td align="center">218</td>
		      <td align="center">52</td>
		      <td align="center">12</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Jagode</td>
		      <td align="center">150</td>
		      <td align="center">36</td>
		      <td align="center">7</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Kivi</td>
		      <td align="center">230</td>
		      <td align="center">55</td>
		      <td align="center">11</td>
		      <td align="center">1</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Kruške</td>
		      <td align="center">230</td>
		      <td align="center">55</td>
		      <td align="center">12</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Lubenica</td>
		      <td align="center">100</td>
		      <td align="center">24</td>
		      <td align="center">5</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Maline</td>
		      <td align="center">170</td>
		      <td align="center">40</td>
		      <td align="center">8</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Mandarine</td>
		      <td align="center">200</td>
		      <td align="center">48</td>
		      <td align="center">11</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Marelice</td>
		      <td align="center">230</td>
		      <td align="center">54</td>
		      <td align="center">12</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Narandže</td>
		      <td align="center">226</td>
		      <td align="center">54</td>
		      <td align="center">9</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Ribizle (crvene)</td>
		      <td align="center">190</td>
		      <td align="center">45</td>
		      <td align="center">10</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Ribizle (crne)</td>
		      <td align="center">260</td>
		      <td align="center">63</td>
		      <td align="center">14</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Šljive</td>
		      <td align="center">245</td>
		      <td align="center">58</td>
		      <td align="center">14</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Trešnje</td>
		      <td align="center">240</td>
		      <td align="center">57</td>
		      <td align="center">13</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
	      </tbody>
	      </table>
<br><h4><b>POVRCE</b></h4>
<tbody>
	<table>
		    <tr>
		      <td width="148" height="20"><strong>Namirnica (100 g)</strong></td>
		      <td colspan="2" align="center"><strong>Energija</strong></td>
		      <td width="34" align="center"><strong>UH</strong></td>
		      <td width="105" align="center"><strong>Belančevine</strong></td>
		      <td width="73" align="center"><strong>Masti</strong></td>
	        </tr>
		    <tr>
		      <td height="10"><b>POVRCE</b></td>
		      <td width="34" align="center">kJ</td>
		      <td width="36" align="center">kcal</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
	        </tr>
		    <tr>
		      <td height="22">Artičoke</td>
		      <td align="center">90</td>
		      <td align="center">23</td>
		      <td align="center">5</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Brokula</td>
		      <td align="center">140</td>
		      <td align="center">33</td>
		      <td align="center">4</td>
		      <td align="center">3</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Cvekla</td>
		      <td align="center">150</td>
		      <td align="center">37</td>
		      <td align="center">8</td>
		      <td align="center">2</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Celer</td>
		      <td align="center">159</td>
		      <td align="center">38</td>
		      <td align="center">7</td>
		      <td align="center">2</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Pasulj</td>
		      <td align="center">480</td>
		      <td align="center">110</td>
		      <td align="center">21</td>
		      <td align="center">7</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Grašak</td>
		      <td align="center">389</td>
		      <td align="center">93</td>
		      <td align="center">14</td>
		      <td align="center">7</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Kelj</td>
		      <td align="center">190</td>
		      <td align="center">46</td>
		      <td align="center">5</td>
		      <td align="center">4</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Krastavci</td>
		      <td align="center">42</td>
		      <td align="center">10</td>
		      <td align="center">2</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Kupus (kiseli)</td>
		      <td align="center">109</td>
		      <td align="center">26</td>
		      <td align="center">4</td>
		      <td align="center">2</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Kupus (slatki)</td>
		      <td align="center">218</td>
		      <td align="center">52</td>
		      <td align="center">7</td>
		      <td align="center">4</td>
		      <td align="center">1</td>
	        </tr>
		    <tr>
		      <td height="22">Luk</td>
		      <td align="center">175</td>
		      <td align="center">42</td>
		      <td align="center">9</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Šargarepa</td>
		      <td align="center">146</td>
		      <td align="center">35</td>
		      <td align="center">7</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Paprika</td>
		      <td align="center">117</td>
		      <td align="center">28</td>
		      <td align="center">5</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Patlidžan</td>
		      <td align="center">110</td>
		      <td align="center">26</td>
		      <td align="center">5</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Paradajz</td>
		      <td align="center">80</td>
		      <td align="center">19</td>
		      <td align="center">3</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Šampinjoni</td>
		      <td align="center">101</td>
		      <td align="center">24</td>
		      <td align="center">3</td>
		      <td align="center">3</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Španać</td>
		      <td align="center">96</td>
		      <td align="center">23</td>
		      <td align="center">2</td>
		      <td align="center">2</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Zelena salata</td>
		      <td align="center">59</td>
		      <td align="center">14</td>
		      <td align="center">2</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Zelje</td>
		      <td align="center">100</td>
		      <td align="center">25</td>
		      <td align="center">4</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
	        </table>
	      </tbody>
<br><h4><b>SLATKISI</b></h4>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
		  <tbody>
		    <tr>
		      <td width="148" height="20"><strong>Namirnica (100 g)</strong></td>
		      <td colspan="2" align="center"><strong>Energija</strong></td>
		      <td width="34" align="center"><strong>UH</strong></td>
		      <td width="107" align="center"><strong>Belančevine</strong></td>
		      <td width="71" align="center"><strong>Masti</strong></td>
	        </tr>
		    <tr>
		      <td height="10"><b>SLATKISI</b></td>
		      <td width="34" align="center">kJ</td>
		      <td width="36" align="center">kcal</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
	        </tr>
		    <tr>
		      <td height="22">Biskvit masni</td>
		      <td align="center">1945</td>
		      <td align="center">462</td>
		      <td align="center">52</td>
		      <td align="center">5</td>
		      <td align="center">26</td>
	        </tr>
		    <tr>
		      <td height="22">Bomboni tvrdi obični</td>
		      <td align="center">1630</td>
		      <td align="center">390</td>
		      <td align="center">91</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Bomboni voćni</td>
		      <td align="center">1220</td>
		      <td align="center">292</td>
		      <td align="center">73</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Čokolada mlečna</td>
		      <td align="center">2355</td>
		      <td align="center">563</td>
		      <td align="center">55</td>
		      <td align="center">9</td>
		      <td align="center">33</td>
	        </tr>
		    <tr>
		      <td height="22">Čokolada za kuvanje</td>
		      <td align="center">2355</td>
		      <td align="center">564</td>
		      <td align="center">63</td>
		      <td align="center">14</td>
		      <td align="center">28</td>
	        </tr>
		    <tr>
		      <td height="22">Čokoladni bomboni</td>
		      <td align="center">1985</td>
		      <td align="center">490</td>
		      <td align="center">68</td>
		      <td align="center">5</td>
		      <td align="center">22</td>
	        </tr>
		    <tr>
		      <td height="22">Čokoladni namaz - nutella</td>
		      <td align="center">2220</td>
		      <td align="center">534</td>
		      <td align="center">59</td>
		      <td align="center">7</td>
		      <td align="center">30</td>
	        </tr>
		    <tr>
		      <td height="22">Guma za žvakanje</td>
		      <td align="center">1170</td>
		      <td align="center">280</td>
		      <td align="center">70</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Gumeni bomboni</td>
		      <td align="center">1450</td>
		      <td align="center">345</td>
		      <td align="center">88</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Kakao prah</td>
		      <td align="center">970</td>
		      <td align="center">232</td>
		      <td align="center">55</td>
		      <td align="center">20</td>
		      <td align="center">14</td>
	        </tr>
		    <tr>
		      <td height="22">Keks sa čokoladnim preljevom</td>
		      <td align="center">2200</td>
		      <td align="center">530</td>
		      <td align="center">68</td>
		      <td align="center">6</td>
		      <td align="center">28</td>
	        </tr>
		    <tr>
		      <td height="22">Marmelada</td>
		      <td align="center">1090</td>
		      <td align="center">261</td>
		      <td align="center">66</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Med</td>
		      <td align="center">1275</td>
		      <td align="center">303</td>
		      <td align="center">81</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Napolitanke</td>
		      <td align="center">2305</td>
		      <td align="center">550</td>
		      <td align="center">62</td>
		      <td align="center">4</td>
		      <td align="center">32</td>
	        </tr>
		    <tr>
		      <td height="22">Piškoti</td>
		      <td align="center">1635</td>
		      <td align="center">393</td>
		      <td align="center">70</td>
		      <td align="center">12</td>
		      <td align="center">7</td>
	        </tr>
		    <tr>
		      <td height="22">Plazma keks</td>
		      <td align="center">1810</td>
		      <td align="center">440</td>
		      <td align="center">70</td>
		      <td align="center">12</td>
		      <td align="center">12</td>
	        </tr>
		    <tr>
		      <td height="22">Puding u prahu</td>
		      <td align="center">1600</td>
		      <td align="center">380</td>
		      <td align="center">95</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Šećer kristal</td>
		      <td align="center">1650</td>
		      <td align="center">391</td>
		      <td align="center">100</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
	      </tbody>
	  </table>
	
<br><h4><b>PICE</b></h4>
<tbody>
<table>
		    <tr>
		      <td width="148" height="20"><strong>Namirnica (100 g)</strong></td>
		      <td colspan="2" align="center"><strong>Energija</strong></td>
		      <td width="34" align="center"><strong>UH</strong></td>
		      <td width="107" align="center"><strong>Belančevine</strong></td>
		      <td width="71" align="center"><strong>Masti</strong></td>
	        </tr>
		    <tr>
		      <td height="10"><b>PICE</b></td>
		      <td width="35" align="center">kJ</td>
		      <td width="35" align="center">kcal</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
		      <td align="center">g</td>
	        </tr>
		    <tr>
		      <td height="22">Limunada</td>
		      <td align="center">210</td>
		      <td align="center">49</td>
		      <td align="center">12</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Sok od jabuke</td>
		      <td align="center">200</td>
		      <td align="center">47</td>
		      <td align="center">12</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Sok od narandže</td>
		      <td align="center">200</td>
		      <td align="center">47</td>
		      <td align="center">11</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Sok od grejpa</td>
		      <td align="center">170</td>
		      <td align="center">40</td>
		      <td align="center">9</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Sok od ribizle</td>
		      <td align="center">210</td>
		      <td align="center">50</td>
		      <td align="center">12</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Sok od šargarepe</td>
		      <td align="center">120</td>
		      <td align="center">28</td>
		      <td align="center">6</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Sok od grožđa</td>
		      <td align="center">300</td>
		      <td align="center">71</td>
		      <td align="center">18</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Svetlo pivo</td>
		      <td align="center">190</td>
		      <td align="center">45</td>
		      <td align="center">4</td>
		      <td align="center">1</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Crno vino</td>
		      <td align="center">280</td>
		      <td align="center">66</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Belo vino</td>
		      <td align="center">290</td>
		      <td align="center">70</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Rakija</td>
		      <td align="center">770</td>
		      <td align="center">185</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Penušavo vino</td>
		      <td align="center">350</td>
		      <td align="center">84</td>
		      <td align="center">3</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
		    <tr>
		      <td height="22">Viski</td>
		      <td align="center">1050</td>
		      <td align="center">250</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
		      <td align="center">0</td>
	        </tr>
	        </table>
	      </tbody>

<br><br><br><br><br>




</div>


  
<div class="content" style=" float:left; margin-right:7px; margin-top:2px; width:20%; height:100%;">
<div style="overflow:auto">
    <form method="post">
    <br>
   <h3> Kalkulator kalorija</h3>
    <br>
 Izaberi pol:
 <Select name="pol" style="background-color: #4e9a06;" ><br>
 <option>Muski</option>
 <option>Zenski</option>
 </select><br><br>
 Godine:
 <input name="godine" type="text" style="color: #000000;"> god.<br><br>
 Tezina:
 <input name="tezina" type="text" style="color: #000000;">kg.<br><br>
 Visina:
 <input name="visina" type="text" style="color: #000000;">cm.<br><br>
 <div class="form-group">
 <input type="Submit" value="Izracunaj" class="btn btn-primary" style="background-color: #4e9a06;">
 </div>
 
 <?php
 if(isset($_POST['godine'])){
     $godine = $_POST['godine'];
 }
 if(isset($godine)){
   //  echo $godine;
 }
 
 if(isset($_POST['tezina'])){
     $tezina = $_POST['tezina'];
 }
 if(isset($tezina)){
 //    echo "Vasa tezina $tezina";
 }

 if(isset($_POST['visina'])){
    $visina = $_POST['visina'];
 }
 if(isset($visina)){
 //     echo $visina;
 }
 
 if(isset($_POST['pol'])){
     $pol = $_POST['pol'];
 }
 if(isset($pol)){
   // echo $pol;
 }
 $kalorije="0.0215183";
 //$pol=$_POST['pol'];
 $pol = isset( $_POST['pol'])? $_POST['pol']: '';

 
 
 switch ($pol){
 case 'Zenski':
 $pol= 655 + (9.6 * $tezina ) + (1.8 * $visina) - (4.7 * $godine);
 echo "<p >Vasa procenjena stopa metabolizma je $pol .</p>";
 echo "<p>To znaci da vam je potrebno $pol kalorija dnevno da bi ste odrzali vasu tezinu.</p>";
 
 break;
 case 'Muski':
 $pol= 66 + (13.7 *$tezina) + (5 * $visina) - (6.8 * $godine);
 echo "<p>Vasa procenjena stopa metabolizma je $pol .</p>";
 echo "<p>To znaci da vam je potrebno $pol kalorija dnevno da bi ste odrzali vasu tezinu.</p>";
 
 }
?>

</div>
<div >
<h2>Postavite pitanje nutricionisti</h2>

Ime:
 <input name="ime" type="text" style="color: #000000;"><br><br>
 Email:
 <input name="email" type="text" style="color: #000000;"><br><br>
 Poruka:
 <input name="text" type="text" style="color: #000000;"><br><br>
 <div class="form-group">
 <input  class="btn btn-primary" style="background-color: #4e9a06;">
 </div>

</div></div>


</div>
 </div>

<footer><?php include './footer.php';?></footer>


</div>
</div>


</body>
</html>