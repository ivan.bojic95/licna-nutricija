
<!DOCTYPE html>
<head>
<title>Kontakt</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="templates/css/nav.css">
<style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>

</head>
<body style="display:flex; 
  flex-direction:column;" >


<div class="content" align="center" style="width: 100%; height: 100%; background-image: url('templates/img/pozadinahrana.jpg'); 
background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">
<h4 style="color:white; float: left; ">Vreme je da zapocnete zdraviji zivot!</h4>

<div class="header">
  <a href="index.php"><img src="templates/img/logopg.png" alt="logo" 
 style=" width:50px; align:left; ">Nutricionista  </a>
  <div class="header-right">
    <a  href="./index.php">Pocetna</a>
    <a class="active" href="./Kontakt.php">Kontakt</a>
    <a  href="./O_nama.php">O nama</a>
  </div>
</div>


<div class="content" style=" float:left; margin-right:7px; margin-top:2px; width:20%; height:100%;">
<div style="overflow:auto">
  <div class="menu">
    <a href="./Uloguj_se.php">Uloguj se</a>
    <a href="./Registruj_se.php">Registruj se</a>  
   
  </div>
</div>
</div>

<div class="content" style=" float:right; margin-top:10px; margin-bottom:5px; width:77%; height:100%;">
<div class=" text-center">
  <h1>Kontakt:</h1>
</div>

<p>Mozete nas pronaci na adresi: Lopa� bb, Po�ega</p>
<p>Kontakt telefon je: (+381)655396758</p>
<p>E-mail: bojic.ivan111@gmail.com</p>

<br><br>
<div id="map"></div>
    <script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: 43.799879, lng: 20.129589};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 4, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
    
    <?php 
	$url = 'https://maps.googleapis.com/maps/api/timezone/json?location=39.6034810,-119.6822510&timestamp=1331161200&key=YOUR_API_KEY
';
	

$client = curl_init();
curl_setopt($client, CURLOPT_URL, $url);
curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);

$response = curl_exec($client);
//$http_status = curl_getinfo($client, CURLINFO_HTTP_CODE);
curl_close($client);
?>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap">
    </script>
   <script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap" async defer></script>

<br><br><br><br><br><br><br><br><br>
</div>

<div style="clear:both"></div>

<footer><?php include './footer.php';?></footer>


</div>
</div>


</body>
</html>