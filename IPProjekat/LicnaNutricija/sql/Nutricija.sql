-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 01, 2020 at 09:53 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Nutricija`
--

-- --------------------------------------------------------

--
-- Table structure for table `hrana`
--

CREATE TABLE `hrana` (
  `idhrane` int(8) NOT NULL,
  `idosobe` int(8) DEFAULT NULL,
  `naziv` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `kalorije` int(8) NOT NULL,
  `kolicina` int(8) NOT NULL,
  `datum` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hrana`
--

INSERT INTO `hrana` (`idhrane`, `idosobe`, `naziv`, `kalorije`, `kolicina`, `datum`) VALUES
(2, 2, 'cokolada', 600, 1, '2020-06-28'),
(5, 1, 'keks', 200, 2, '2020-06-29');

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lozinka` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kreirano` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id`, `email`, `lozinka`, `kreirano`) VALUES
(1, 'lidija@gmail.com', '123456', '2020-06-24 23:35:09'),
(6, 'nikolartn@gmail.com', '$2y$10$5Pt9vtGPA4UizanOTL0oXus1o8RnatIF/5w.F2MNUjMTjJ7qCaqbK', '2020-06-29 00:14:57');

-- --------------------------------------------------------

--
-- Table structure for table `osoba`
--

CREATE TABLE `osoba` (
  `ID` int(8) NOT NULL,
  `Ime` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `Prezime` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `Godine` int(8) NOT NULL,
  `Tezina` int(8) NOT NULL,
  `Visina` int(8) NOT NULL,
  `Pol` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `osoba`
--

INSERT INTO `osoba` (`ID`, `Ime`, `Prezime`, `Godine`, `Tezina`, `Visina`, `Pol`) VALUES
(6, 'nikola', 'djurkovic', 22, 88, 187, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hrana`
--
ALTER TABLE `hrana`
  ADD PRIMARY KEY (`idhrane`),
  ADD UNIQUE KEY `idhrane` (`idhrane`),
  ADD UNIQUE KEY `idosobe` (`idosobe`),
  ADD UNIQUE KEY `idosobe_2` (`idosobe`);

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ime` (`email`);

--
-- Indexes for table `osoba`
--
ALTER TABLE `osoba`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hrana`
--
ALTER TABLE `hrana`
  MODIFY `idhrane` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `osoba`
--
ALTER TABLE `osoba`
  ADD CONSTRAINT `osoba_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `korisnici` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
