<?php
// Include config file
require_once "db.php";

// Define variables and initialize with empty values
$email = $lozinka = $potvrdi_lozinku = "";
$email_err = $lozinka_err = $potvrdi_lozinku_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    
    // Validate username
    if(empty(trim($_POST["email"]))){
        $email_err = "Unesite email.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM korisnici WHERE email = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["email"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $email_err = "Email vec postoji.";
                } else{
                    $email = trim($_POST["email"]);
                }
            } else{
                echo "Nesto nije u redu, pokusajte kasnije opet.";
            }
            
            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Validate password
    if(empty(trim($_POST["lozinka"]))){
        $lozinka_err = "Unesite lozinku";
    } elseif(strlen(trim($_POST["lozinka"])) < 6){
        $lozinka_err = "Lozinka mora imati najmanje 6 karaktera";
    } else{
        $lozinka = trim($_POST["lozinka"]);
    }
    
    // Validacija potvrdi lozinku
    if(empty(trim($_POST["potvrdi_lozinku"]))){
        $potvrdi_lozinku_err = "Potvrdite lozinku";
    } else{
        $potvrdi_lozinku = trim($_POST["potvrdi_lozinku"]);
        if(empty($lozinka_err) && ($lozinka != $potvrdi_lozinku)){
            $potvrdi_lozinku_err = "Lozinka ne postoji.";
        }
    }
    
    // Check input errors before inserting in database
    if(empty($email_err) && empty($lozinka_err) && empty($potvrdi_lozinku_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO korisnici (email, lozinka) VALUES (?, ?)";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
            
            // Set parameters
            $param_username = $email;
           $param_password = password_hash($lozinka, PASSWORD_DEFAULT); // Creates a password hash
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: ./Uloguj_se.php");
            } else{
                echo "Nesto nije u redu, pokusajte kasnije.";
            }
            
            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Close connection
    mysqli_close($link);
}
?>
<!DOCTYPE html>
<head>
<title>Registruj se</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="templates/css/nav.css">
</head>
<body>


<div class="container" align="center" style="width: 100%; background-attachment: fixed; height: 100%; background-image: url('templates/img/pozadinahrana.jpg'); background-size: 120%;">
<h4 style="color:white; float: left; ">Vreme je da zapocnete zdraviji zivot!</h4>

<header><?php include './header.php';?></header>


<div class="content" style=" float:left; margin-right:7px; margin-top:10px; width:20%; height:100%;">
<div style="overflow:auto">
  <div class="menu">
<!--     <h5>Nemas nalog?</h5> -->
<!--     <a href="./Registruj_se.php">Registruj se</a>    -->
  </div>
</div>
	
	
</div>
<div class="content" style=" float:right; margin-top:10px; margin-bottom:5px; width:77%; height:100%;">


        <h2>Registruj se</h2>
        <p>Molim vas popunite polja kako bi kreirali nalog</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                <label>Email</label>
                <input type="text" name="email" class="form-control" value="<?php echo $email; ?>">
                <span class="help-block"><?php echo $email_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($lozinka_err)) ? 'has-error' : ''; ?>">
                <label>Lozinka</label>
                <input type="password" name="lozinka" class="form-control" value="<?php echo $lozinka; ?>">
                <span class="help-block"><?php echo $lozinka_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($potvrdi_lozinku_err)) ? 'has-error' : ''; ?>">
                <label>Potvrdi lozinku</label>
                <input type="password" name="potvrdi_lozinku" class="form-control" value="<?php echo $potvrdi_lozinku; ?>">
                <span class="help-block"><?php echo $potvrdi_lozinku_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Potvrdi" style="background-color: #4e9a06;">
                <input type="reset" class="btn btn-default" value="Resetuj" href="Registruj_se.php">
            </div>
            <p>Vec imas nalog? <a href="Uloguj_se.php">Prijavi se ovde!</a>.</p>
        </form>
    </div> 

<div style="clear:both"></div>
<footer><?php include './footer.php';?></footer>


</div>
</div>


</body>
</html>