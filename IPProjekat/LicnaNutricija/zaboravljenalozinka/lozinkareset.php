<link href="nav.css" rel="stylesheet" type="text/css">
<script>
function validate_password_reset() {
	if((document.getElementById("lozinka").value == "") && (document.getElementById("potvrdi_lozinku").value == "")) {
		document.getElementById("validation-message").innerHTML = "Unesi novu lozinku!"
		return false;
	}
	return true
}
</script>
<form name="frmForgot" id="frmForgot" method="post" onSubmit="return validate_password_reset();">

	<?php if(!empty($success_message)) { ?>
	<div class="success_message"><?php echo $success_message; ?></div>
	<?php } ?>

	<div id="validation-message">
		<?php if(!empty($error_message)) { ?>
	<?php echo $error_message; ?>
	<?php } ?>
	</div>

	<div class="field-group">
		<div><label for="Password">Lozinka</label></div>
		<div><input type="password" name="lozinka" id="lozinka" class="input-field"> Or</div>
	</div>
	
	<div class="field-group">
		<div><label for="email">Potvrdi lozinku</label></div>
		<div><input type="password" name="potvrdi_lozinku" id="potvrdi_lozinku" class="input-field"></div>
	</div>
	
	<div class="field-group">
		<div><input type="submit" name="resetuj_lozinku" id="resetuj_lozinku" value="Submit" class="form-submit-button"></div>
	</div>	
</form>
				