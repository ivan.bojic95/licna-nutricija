<?php
$connect = mysqli_connect("localhost", "root", "", "Nutricija");
$output = '';
$sql = "SELECT * FROM hrana ORDER BY idhrane DESC";
$result = mysqli_query($connect, $sql);
$row = isset($row['naziv']) ? count($row['naziv']) : 0;
$output .= '<h2>Vasa tabela kalorija</h2>
      <div class="table-responsive">
           <table class="table table-bordered">
                <tr> 
                     <th width="10%">ID</th>
                     <th width="10%">Naziv</th>
                     <th width="40%">Kolicina(g)</th>
                     <th width="10%">Kalorije</th>
                     <th width="10%">Masti</th>
                     <th width="10%">Proteini</th>
                     <th width="10%">Ugljenihidrati</th>
                     <th width="40%">Datum</th>
                     <th width="10%">Izaberi:</th>
                </tr>';

$rows = mysqli_num_rows($result);
if($rows > 0)
{
    if($rows > 10)
    {
        $delete_records = $rows - 10;
        $delete_sql = "DELETE FROM hrana LIMIT $delete_records";
        mysqli_query($connect, $delete_sql);
    }
    while($row = mysqli_fetch_array($result))
    {
        $output .= '
                <tr>
                     <td>'.$row["idhrane"].'</td>
                     <td class="naziv" data-id1="'.$row["idhrane"].'" contenteditable>'.$row["naziv"].'</td>
                     <td class="kolicina" data-id2="'.$row["idhrane"].'" contenteditable>'.$row["kolicina"].'</td>
                     <td class="kalorije" data-id2="'.$row["kalorije"].'" contenteditable>'.$row["kalorije"].'</td>
                     <td class="masti" data-id2="'.$row["masti"].'" contenteditable>'.$row["masti"].'</td>
                     <td class="proteini" data-id2="'.$row["proteini"].'" contenteditable>'.$row["proteini"].'</td>
                     <td class="ugljenihidrati" data-id2="'.$row["ugljenihidrati"].'" contenteditable>'.$row["ugljenihidrati"].'</td>
                     <td class="datum" data-id2="'.$row["datum"].'" contenteditable>'.$row["datum"].'</td>
                     <td><button type="button" name="delete_btn" data-id3="'.$row["idhrane"].'" class="btn btn-xs btn-danger btn_delete">x</button></td>
                </tr>
           ';
    }
    $output .= '
           <tr>
                <td></td>
                <td id="naziv" contenteditable></td>
                <td id="kolicina" contenteditable></td>
                <td id="kalorije" contenteditable></td>
                <td id="masti" contenteditable></td>
                <td id="proteini" contenteditable></td>
                <td id="ugljenihidrati" contenteditable></td>
                <td id="datum" contenteditable></td>
                <td><button type="button" name="btn_add" id="btn_add" class="btn btn-xs btn-success">+</button></td>
           </tr>
      ';
} 
else
{
    $output .= '
				<tr>
				   <td> '.$row["idhrane"].'</td>
                  <td class="naziv" data-id1="'.$row["idhrane"].'" contenteditable>'.$row["naziv"].'</td>
                   <td class="kolicina" data-id2="'.$row["idhrane"].'" contenteditable>'.$row["kolicina"].'</td>
                   <td class="kalorije" data-id2="'.$row["kalorije"].'" contenteditable>'.$row["kalorije"].'</td>
                   <td class="masti" data-id2="'.$row["masti"].'" contenteditable>'.$row["masti"].'</td>
                   <td class="proteini" data-id2="'.$row["proteini"].'" contenteditable>'.$row["proteini"].'</td>
                   <td class="ugljenihidrati" data-id2="'.$row["ugljenihidrati"].'" contenteditable>'.$row["ugljenihidrati"].'</td>
                   <td class="datum" data-id2="'.$row["datum"].'" contenteditable>'.$row["datum"].'</td>

			   </tr>';
}
$output .= '</table>
      </div>';
echo $output;
?>