
<?php
class LiveTable_model extends CI_Model
{
 function load_data()
 {
  $this->db->order_by('idhrane', 'DESC');
  $query = $this->db->get('hrana');
  return $query->result_array();
 }

 function insert($data)
 {
  $this->db->insert('hrana', $data);
 }

 function update($data, $idhrane)
 {
  $this->db->where('idhrane', $idhrane);
  $this->db->update('hrana', $data);
 }

 function delete($idhrane)
 {
  $this->db->where('idhrane', $idhrane);
  $this->db->delete('hrana');
 }
}
?>
