
<!DOCTYPE html>
<head>
<title>O nama</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="templates/css/nav.css">
</head>
<body>


<div class="content" align="center" style="width: 100%; height: 100%; background-image: url('templates/img/pozadinahrana.jpg'); 
background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">
<h4 style="color:white; float: left; ">Vreme je da zapocnete zdraviji zivot!</h4>

<div class="header">
  <a href="index.php"><img src="templates/img/logopg.png" alt="logo" 
 style=" width:50px; align:left; ">Nutricionista  </a>
  <div class="header-right">
    <a  href="./index.php">Pocetna</a>
    <a href="./Kontakt.php">Kontakt</a>
    <a class="active" href="./O_nama.php">O nama</a>
  </div>
</div>


<div class="content" style=" float:left; margin-right:7px; margin-top:2px; width:20%; height:100%;">
<div style="overflow:auto">
  <div class="menu">
    <a href="./Uloguj_se.php">Uloguj se</a>
    <a href="./Registruj_se.php">Registruj se</a>   
  </div>
</div>
	
	
</div>
<div class="content" style=" float:right; margin-top:10px; margin-bottom:5px; width:77%; height:100%;">
<div class=" text-center">
  <h1>O nama:</h1>
</div>
Nutricionistički studio je osnovan 2010. godine i do sada je kroz njega prošlo više od 10000 ljudi. Njihova iskustva u radu sa nama su ujedno i naša najbolja preporuka.

Noseći se sopstvenim iskustvom, svaka osoba sa prekomernom težinom treba da redukuje telesnu težinu, da je adekvatno održava, da pronađe motiv i postavi realan cilj, da promeni navike i nauči da prepozna uspeh. Ako sve to budete uspeli bićete pre svega zdraviji, a kasnije i zadovoljniji.

S ponosom ističemo da naše personalizovane dijete (ne postoje dve iste dijete), direktan kontakt sa pacijentima i interaktivan način rada na kraju uvek dovedu do željenog rezultata.

Radimo individualne jelovnike za sve uzraste, redukcione i medicinske dijete (insulinska rezistencija, hipertenzija, psorijaza, gastritis, anemija, hašimoto, helikoibakterija, kandida, kod dijabetesa, hiperholesterolemije, artritisa i dr.), a izradjujemo i jelovnike za one koji nemaju nikakav problem vec zele da se zdravo hrane.

Od pravilne ishrane koristi mogu biti višestruke: jačanje imuniteta, zaštita organizma od bolesti, veći procenat mišića, manji procenat masti, odgovarajuća telesna težina, smanjen rizik nastanka kardiovaskularnih bolesti, bolji san i lakše buđenje ujutru, psihička stabilnost, fizička spremnost, bolja pokretljivost, smanjeni otoci i bolovi u zglobovima, plodnost, duži život i niz drugih prednosti.

Nutricionistički studio Ane Petrović Vam pruža edukaciju i savete o zdravim navikama koje bi svako od nas trebalo da usvoji. Ključ zdravlja i uspeha je u Vama, a mi smo tu da Vam pomognemo da dodjete do željenog cilja.
<br><br><br><br><br><br><br><br><br>

</div>

<div style="clear:both"></div>
<footer><?php include './footer.php';?></footer>


</div>
</div>


</body>
</html>