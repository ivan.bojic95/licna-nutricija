-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 02, 2020 at 10:07 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Nutricija`
--

-- --------------------------------------------------------

--
-- Table structure for table `hrana`
--

CREATE TABLE `hrana` (
  `idhrane` int(8) NOT NULL,
  `idosobe` int(8) DEFAULT NULL,
  `naziv` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `kalorije` int(8) NOT NULL,
  `kolicina` int(8) NOT NULL,
  `masti` int(8) NOT NULL,
  `proteini` int(8) NOT NULL,
  `ugljenihidrati` int(8) NOT NULL,
  `datum` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hrana`
--

INSERT INTO `hrana` (`idhrane`, `idosobe`, `naziv`, `kalorije`, `kolicina`, `masti`, `proteini`, `ugljenihidrati`, `datum`) VALUES
(18, NULL, 'keks', 100, 200, 40, 70, 20, '2020-07-01'),
(19, NULL, 'cokolada', 200, 100, 30, 40, 100, '2020-07-01'),
(20, NULL, 'slatko', 400, 300, 20, 150, 300, '2020-07-01');

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lozinka` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kreirano` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id`, `email`, `lozinka`, `kreirano`) VALUES
(1, 'lidija@gmail.com', '123456', '2020-06-24 23:35:09'),
(6, 'nikolartn@gmail.com', '$2y$10$5Pt9vtGPA4UizanOTL0oXus1o8RnatIF/5w.F2MNUjMTjJ7qCaqbK', '2020-06-29 00:14:57'),
(8, 'Bezimenkoprezimenjakovic@gmail.com', '$2y$10$fUT07gM5HYCSCWA82sgqLOByMeZcaSWQUx/kG/Ui73wqXbDlKVesm', '2020-07-02 08:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `osoba`
--

CREATE TABLE `osoba` (
  `ID` int(8) NOT NULL,
  `Ime` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `Prezime` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `Godine` int(8) NOT NULL,
  `Tezina` int(8) NOT NULL,
  `Visina` int(8) NOT NULL,
  `Pol` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `osoba`
--

INSERT INTO `osoba` (`ID`, `Ime`, `Prezime`, `Godine`, `Tezina`, `Visina`, `Pol`) VALUES
(6, 'nikola', 'djurkovic', 22, 88, 187, 2);

-- --------------------------------------------------------

--
-- Table structure for table `osobahrana`
--

CREATE TABLE `osobahrana` (
  `osobaid` int(8) NOT NULL,
  `hranaid` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hrana`
--
ALTER TABLE `hrana`
  ADD PRIMARY KEY (`idhrane`),
  ADD UNIQUE KEY `idhrane` (`idhrane`),
  ADD UNIQUE KEY `idosobe` (`idosobe`),
  ADD UNIQUE KEY `idosobe_2` (`idosobe`);

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ime` (`email`);

--
-- Indexes for table `osoba`
--
ALTER TABLE `osoba`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `osobahrana`
--
ALTER TABLE `osobahrana`
  ADD UNIQUE KEY `osobaid` (`osobaid`,`hranaid`),
  ADD KEY `hranaid` (`hranaid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hrana`
--
ALTER TABLE `hrana`
  MODIFY `idhrane` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `osoba`
--
ALTER TABLE `osoba`
  ADD CONSTRAINT `osoba_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `korisnici` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `osobahrana`
--
ALTER TABLE `osobahrana`
  ADD CONSTRAINT `osobahrana_ibfk_1` FOREIGN KEY (`hranaid`) REFERENCES `hrana` (`idhrane`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `osobahrana_ibfk_2` FOREIGN KEY (`osobaid`) REFERENCES `osoba` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
